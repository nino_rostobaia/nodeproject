const express = require('express');
const app = express();
const port = 3000;


app.get('/', function(req, res) {
    res.send('Hello World!');
})



app.get('/about', function(req, res) {
    res.send('About Page');
})


app.get('/contact', function(req, res) {
    res.send('Contact Page');
})



app.listen(port, function() {
    console.log("server started");
})